$(window).load(function(){
$('#carousel').flexslider({
animation: "slide",
controlNav: false,
animationLoop: false,
slideshow: false,
itemWidth: 75,
itemMargin: 5,
asNavFor: '#slider'
});

$('#slider').flexslider({
animation: "slide",
controlNav: false,
animationLoop: true,
slideshow: false,
sync: "#carousel",
start: function(slider){
$('body').removeClass('loading');
}
});
});
