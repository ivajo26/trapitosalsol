var token = '549710600.1677ed0.4e81b5249ba74bf8b9943b6500e4dada',
    num_photos = 6;

$.ajax({
    url: 'https://api.instagram.com/v1/users/self/',
    dataType: 'jsonp',
    type: 'GET',
    data: {access_token: token},
    success: function(data){
        console.log(data);
        $('#user').append(data.data.username);
        $('#post').append(data.data.counts.media);
        $('#follower').append(data.data.counts.followed_by);
        $('#following').append(data.data.counts.follows);
    },
    error: function(data){
        console.log(data);
    }
});

$.ajax({
    url: 'https://api.instagram.com/v1/users/self/media/recent',
    dataType: 'jsonp',
    type: 'GET',
    data: {access_token: token, count: num_photos},
    success: function(data){
        console.log(data);
        for( x in data.data ){
            $('#media').append('<img src="'+data.data[x].images.low_resolution.url+'">');
        }
    },
    error: function(data){
        console.log(data);
    }
});
