var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'contacto';
	locals.formData = req.body || {};
	locals.validationErrors = {};
	locals.enquirySubmitted = false;

	// On POST requests, add the Enquiry item to the database
	view.on('post', { action: 'contact' }, function (next) {

		var newEnquiry = new Enquiry.model();
		var updater = newEnquiry.getUpdateHandler(req);

		updater.process(req.body, {
			flashErrors: true,
			fields: 'name, email, phone, address, pageweb, comment',
			errorMessage: 'There was a problem submitting your enquiry:',
		}, function (err) {
			if (err) {
				locals.validationErrors = err.errors;
			} else {
				locals.enquirySubmitted = true;
			}
			next();
		});
	});

	locals.validationErrors = {};
	locals.NewsletterSubmitted = false;
	view.on('post', { action: 'newsletter' }, function (next) {

		var newNewsletter = new Newsletter.model();
		var updater = newNewsletter.getUpdateHandler(req);

		updater.process(req.body, {
			flashErrors: true,
			fields: 'email',
			errorMessage: 'There was a problem submitting your Newsletter:',
		}, function (err) {
			if (err) {
				locals.validationErrors = err.errors;
			} else {
				locals.NewsletterSubmitted = true;
			}
			next();
		});
	});
	view.render('contact');
};
