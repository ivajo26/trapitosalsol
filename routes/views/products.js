'use strict';
var keystone = require('keystone');
var async = require('async');

exports = module.exports = function (req, res) {
  var view = new keystone.View(req, res);
  var locals = res.locals;

  locals.section = 'store';
  locals.filters = {
		products: req.params.products
	};
  locals.data = {
    products: [],
		lineas: [],
	};

  // Load all Lineas
	view.on('init', function (next) {

		keystone.list('Linea').model.find().sort('name').exec(function (err, results) {

			if (err || !results.length) {
				return next(err);
			}

			locals.data.lineas = results;

			// Load the counts for each category
			async.each(locals.data.lineas, function (linea, next) {

				keystone.list('ProductCategory').model.count().where('categories').in([linea.id]).exec(function (err, count) {
					linea.categorytCount = count;
					next(err);
				});

			}, function (err) {
				next(err);
			});
		});
	});

  view.on('init', function (next) {

		if (req.params.products) {
			keystone.list('Product').model.findOne({ slug: locals.filters.products }).exec(function (err, result) {
				locals.data.products = result;
				next(err);
			});
		} else {
			next();
		}
	});

  view.render('products');
};
