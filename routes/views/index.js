var keystone = require('keystone');
var Newsletter = keystone.list('Newsletter');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.data={
		Categorieslinea:[]
	}

	locals.section = 'home';

	view.on('init', function (next) {

			keystone.list('ProductCategory').model.find().populate('line').limit(6).exec(function (err, result) {
				locals.data.Categorieslinea = result;
				next(err);
			});
	});
	locals.validationErrors = {};
	locals.NewsletterSubmitted = false;
	view.on('post', { action: 'newsletter' }, function (next) {

		var newNewsletter = new Newsletter.model();
		var updater = newNewsletter.getUpdateHandler(req);

		updater.process(req.body, {
			flashErrors: true,
			fields: 'email',
			errorMessage: 'There was a problem submitting your Newsletter:',
		}, function (err) {
			if (err) {
				locals.validationErrors = err.errors;
			} else {
				locals.NewsletterSubmitted = true;
			}
			next();
		});
	});



	// Render the view
	view.render('index');
};
