'use strict';
var keystone = require('keystone');
var async = require('async');

exports = module.exports = function (req, res) {
  var view = new keystone.View(req, res);
  var locals = res.locals;

  locals.section = 'store';
  locals.filters = {
		linea: req.params.linea,
    category: req.params.category,
	};
  locals.data = {
    products: [],
		lineas: [],
	};

  // Load all Lineas
	view.on('init', function (next) {

		keystone.list('Linea').model.find().sort('name').exec(function (err, results) {

			if (err || !results.length) {
				return next(err);
			}

			locals.data.lineas = results;

			// Load the counts for each category
			async.each(locals.data.lineas, function (linea, next) {

				keystone.list('ProductCategory').model.count().where('categories').in([linea.id]).exec(function (err, count) {
					linea.categorytCount = count;
					next(err);
				});

			}, function (err) {
				next(err);
			});
		});
	});

  view.on('init', function (next) {

		if (req.params.linea) {
			keystone.list('Linea').model.findOne({ key: locals.filters.linea }).exec(function (err, result) {
				locals.data.linea = result;
				next(err);
			});
		} else {
			next();
		}
	});

  view.on('init', function (next) {

		if (locals.data.linea) {
			keystone.list('ProductCategory').model.find({ line: locals.data.linea}).exec(function (err, result) {
				locals.data.Categorieslinea = result;
				next(err);
			});
		} else {
			next();
		}
	});

  view.on('init', function (next) {

		if (req.params.category) {
			keystone.list('ProductCategory').model.findOne({ key: locals.filters.category }).exec(function (err, result) {
				locals.data.category = result;
				next(err);
			});
		} else {
			next();
		}
	});

  view.on('init', function (next) {

		var q = keystone.list('Product').paginate({
			page: req.query.page || 1,
			perPage: 10,
			maxPages: 10,
			filters: {
				state: 'Publicado',
			},
		})
			.sort('-publishedDate');

		if (locals.data.linea) {
      if (locals.data.category) {
        q.where('categories').in([locals.data.category]);
      }else{
        var cats = Array();
        locals.data.Categorieslinea.forEach (function(cat) {
  			     cats.push(cat);
  		  });
  			q.where('categories').in(cats);
      }
		}

		q.exec(function (err, results) {
      console.log(results);
			locals.data.products= results;
			next(err);
		});
	});

  locals.validationErrors = {};
	locals.NewsletterSubmitted = false;
	view.on('post', { action: 'newsletter' }, function (next) {

		var newNewsletter = new Newsletter.model();
		var updater = newNewsletter.getUpdateHandler(req);

		updater.process(req.body, {
			flashErrors: true,
			fields: 'email',
			errorMessage: 'There was a problem submitting your Newsletter:',
		}, function (err) {
			if (err) {
				locals.validationErrors = err.errors;
			} else {
				locals.NewsletterSubmitted = true;
			}
			next();
		});
	});
  view.render('store');
};
