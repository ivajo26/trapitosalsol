var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ProductCategory Model
 * ==================
 */

var Linea = new keystone.List('Linea', {
	autokey: { path: 'key', from: 'name', unique: true }
});

Linea.add({
	name: { type: String, required: true },
});

Linea.relationship({ ref: 'ProductCategory', path: 'productcategorys', refPath: 'line'});

Linea.register();
