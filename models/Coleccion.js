var keystone = require('keystone');
var Types = keystone.Field.Types;

var Coleccion = new keystone.List('Coleccion', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Coleccion.add({
	name: { type: String, required: true },
});

Coleccion.relationship({ ref: 'Product', path: 'colecciones' });

Coleccion.register();
