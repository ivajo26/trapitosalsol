var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Newsletter Model
 * =============
 */

var Newsletter = new keystone.List('Newsletter', {
	nocreate: true,
	noedit: true,
});

Newsletter.add({
	email: { type: Types.Email, required: true },
	createdAt: { type: Date, default: Date.now },
});

Newsletter.schema.pre('save', function (next) {
	this.wasNew = this.isNew;
	next();
});

Newsletter.defaultSort = '-createdAt';
Newsletter.defaultColumns = 'email, createdAt';
Newsletter.register();
