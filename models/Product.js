var keystone = require('keystone');
var Types = keystone.Field.Types;

var Product = new keystone.List('Product', {
	map: { name: 'name' },
	autokey: { path: 'slug', from: 'name', unique: true },
});

Product.add({
	name: { type: String, required: true },
	state: { type: Types.Select, options: 'Publicado, Sin Publicar', default: 'Sin Publicar', index: true },
  coverImage: { type: Types.CloudinaryImage },
  images: { type: Types.CloudinaryImages},
  publishedDate: { type: Date, default: Date.now },
	categories: { type: Types.Relationship, ref: 'ProductCategory', many: true , index: true},
	coleccion: { type: Types.Relationship, ref: 'Coleccion', many: true, index: true },
});
Product.defaultColumns = 'name, state, publishedDate';
Product.register();
