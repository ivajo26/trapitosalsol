var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ProductCategory Model
 * ==================
 */

var ProductCategory = new keystone.List('ProductCategory', {
	map: { name: 'name' },
	autokey: { path: 'key', from: 'name', unique: true }
});

ProductCategory.add({
	name: { type: String, required: true },
	line: { type: Types.Relationship, ref: 'Linea', index: true},
	coverImage: { type: Types.CloudinaryImage },
});

ProductCategory.register();
